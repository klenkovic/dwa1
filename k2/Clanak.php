<?php

    class Clanak{
	
	public $id;
	public $naslov;
	public $sadrzaj;
	public $autor;
	public $datum;
	public $jeObjavljeno;
	
	public function Clanak($id=""){
	   
	    //$this->_debug_($baza);
	    
	    if($id!=""){
		
		$baza = $this->citanje();
		
		foreach($baza as $red){
		    if($red[0] == $id){
			$this->id = $id;
			$this->naslov = $red[1];
			$this->sadrzaj = $red[2];
			$this->autor = $red[3];
			$this->datum = $red[4];
			$this->jeObjavljeno = $red[5];
		    }
		}
		
		
	    }
	    
	}
	
	public function spremi(){
	    $baza = $this->citanje();
	    $novaBaza = array();
	    foreach($baza as $red){
		if($red[0] == $this->id){
		    $red[5] = $this->jeObjavljeno;
		}
		$novaBaza[] = $red;
	    }
	    //$this->_debug_($baza);
	    $this->pisanje($novaBaza);
	}
	
	public function objavi(){
	    $this->jeObjavljeno = 1;
	    $this->spremi();
	    //$this->_debug_($this->citanje());
	}
	
	public function sakrij(){
	    $this->jeObjavljeno = 0;
	    $this->spremi();
	}
	
	public function link($id){
	    $baza = $this->citanje();
	    foreach ($baza as $red) {
		if($red[0] == $id){
		    echo "<a href=\"z2.php?id=".$id."\">".$red[1]."</a><br />";
		}
	    }
	}
	
	public function prikaz($id){
	    $baza = $this->citanje();
	    ?>

<table cellspacing="5" cellpadding="5">
    <tr>
	<th>ID</th>
	<th>Naslov</th>
	<th>Sadržaj</th>
	<th>Autor</th>
	<th>Datum</th>
	<th>Objavljeno</th>
    </tr>

<?php
	    foreach ($baza as $red) {
		if($red[0] == $id){
		    ?>
    <tr>
	<td><?=$red[0]?></td>
	<td><?=$red[1]?></td>
	<td><?=$red[2]?></td>
	<td><?=$red[3]?></td>
	<td><?=$red[4]?></td>
	<td><?=$red[5]?></td>
    </tr>


		    <?php
		}
	    }
	    ?>
	    </table>
	    <?php
	    
	}
	
	private function citanje(){
	    $baza = array();
	    $c = fopen("clanci.txt", "r");
	    while(!feof($c)){
		$baza[] = explode("-",trim(fgets($c)));
	    }
	    array_pop($baza);
	    fclose($c);
	    return $baza;
	}

	private function pisanje($baza){
	    $c = fopen("clanci.txt", "w");
	    foreach ($baza as $red){
		fwrite($c,  implode("-", $red)."\n");
	    }
	    fclose($c);
	}
	
	public function _debug_($a){
	    echo "<pre>";
	    print_r($a);
	    echo "</pre>";
	}
	
    }


?>