<?php

    function ispisPolja($a){
        echo "<pre>";
        print_r ($a);
        echo "</pre>";
    }

    $proizvod_str = $_POST["proizvod"];
    $velicina_str  = $_POST["velicina"];
    $user     = $_POST["user"];
    
    
    $proizvod = explode(" ",$proizvod_str);
    /* proizvod[]
     * [0] -> ime
     * [1] -> cijena
     * [2] -> valuta
     */
    
    $velicina = explode(" ",$velicina_str);
    /* velicina[]
     * [0] -> ime
     * [1] -> cijena
     * [2] -> valuta
     */

        //ispisPolja($proizvod);
    
     $zaIspis = $user . "\t" . $proizvod[0] . "\t" . $velicina[0] . "\t" . ($proizvod[1] + $velicina[1]) . "\t" . date("d.m.Y.") . "\n";
     
     echo $zaIspis;
     
     
     $nar = fopen("narudzbe.txt", "a");
     
     flock($nar, LOCK_EX);
     if ( fwrite($nar, $zaIspis) ){
         echo "<br /><h2>Uspješan unos</h2>";
     }else{
         echo "<br /><h2>Unos neuspješan.</h2>";
     }
     flock($nar, LOCK_UN);
     fclose($nar);
      
     
     $nar = fopen("narudzbe.txt", "r");
     
     $tablica = array();
     
     while(!feof($nar)){
         array_push($tablica, explode("\t", trim(fgets($nar))));
     }
     
     fclose($nar);
     array_pop($tablica);

?>

<table border="1" cellpadding="5" cellspacing="1">
    <tr>
        <th>Korisnik</th>
        <th>Proizvod</th>
        <th>Veličina</th>
        <th>Ukupna cijena</th>
        <th>Datum</th>
    </tr>
    <?php
        foreach($tablica as $red){
    ?>
    <tr>
        <td><?=$red[0]?></td>
        <td><?=$red[1]?></td>
        <td><?=$red[2]?></td>
        <td><?=$red[3]?></td>
        <td><?=$red[4]?></td>
    </tr>
    <?php
        }
    ?>
</table>