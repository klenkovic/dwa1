﻿<?php
/*
* FUNKCIJA select (padajuca lista)
* ULAZ: $path - putanja do i ime datoteke
		$id - redni broj stupca koji ide u value
		$podaci - array sa brojevima stupaca koji se ispisuju
		$name - ime HTML polja obrasca
* IZLAZ: Sadrzaj padajuce liste / false
* OPIS: Generira padajucu listu iz datoteke
* IZRADIO: Edvin Močibob
* DATUM ZADNJE IZMJENE: 2013-12-04 
*/
function select($path, $id, $podaci, $name) // $name=0 --> $name argument je opcionalan
{
	if(filesize($path) != 0)
	{
	 	$out = array();
		$fh = fopen($path, 'r');
		$out .= '<select name="'.$name.'">';
		while(($red = fgets($fh, 4096)) !== false)
		{
			$redak = explode("\t", $red);
			$out .= '<option value="'.$redak[$id].'">';
			foreach ($podaci as $p)
			{
				$out .= $redak[$p].' ';
			}
			$out .= '</option>';
		}
		$out .= '</select>';
		fclose($fh);
		
		return $out;
	}
	else
	{
		return false;
	}
}
/*
* <select name="zemlje">
*  <option value="HRV">Hrvatska</option>
* </select>
*/
?>