﻿<?php
/*
* FUNKCIJA preselect (padajuca lista sa odabranom stavkom)
* ULAZ: string $path - putanja do i ime datoteke
		int $id - redni broj stupca koji ide u value
		string $sid - redak kojeg trebamo oznaciti (selected ID)
		array $podaci - array sa brojevima stupaca koji se ispisuju
		string $name - ime HTML polja obrasca
* IZLAZ: Sadrzaj padajuce liste / false
* OPIS: Generira padajucu listu sa odabranom stavkom iz datoteke
* IZRADIO: Edvin Močibob
* DATUM ZADNJE IZMJENE: 2013-12-04 
  PRIMJER: 
  $podaci = array();
  $podaci[] = 1;
  preselect('/home/countries.txt', 0, 'BIH', array(1), 'zemlje');
*/
function select($path, $id, $sid, $podaci, $name) // $name=0 --> $name argument je opcionalan
{
	
}
/*
* <select name="zemlje">
*  <option value="HRV" selected>Hrvatska</option>
*  <option value="BIH">BiH</option>
* </select>
*/
?>