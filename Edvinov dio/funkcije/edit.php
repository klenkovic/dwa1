﻿<?php
/*
* FUNKCIJA edit - zamjenjuje jednu ili više vrijednosti u traženom retku
* ULAZ: array $podaci - asoc. array sa brojem stupca i vrijednoscu
* IZLAZ: vraća cijeli redak sa unesenim izmjenama
* OPIS: Mijenja podatke u retku
* KORISTI: find()
* IZRADIO: Edvin Močibob
* DATUM ZADNJE IZMJENE: 2013-12-04 
* PRIMJER:
* edit('/home/osobe.txt', 0, "\t", array(1=>'Ime', 2=>'Prezime'));
*/
function edit($path, $id, $delimiter, $podaci)
{
	// $redak = find
	// za svaki element u polju podaci
	//   izmjeni element u polju $redak
	// return $redak
}
?>