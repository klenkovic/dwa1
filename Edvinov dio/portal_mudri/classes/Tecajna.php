﻿<?php

class Tecajna {

    private $valute=array();
    private $path = 'files/tecajna.txt';
    private $podaci = array();

    public function __construct($valute) {
        $this->valute = explode(',', $valute);
        $fh = fopen($this->path, 'r');
        while (($red = fgets($fh, 4096)) !== false) {
            $redak = explode('-', $red);
            if (in_array($redak[0], $this->valute))
                $this->podaci[$redak[0]] = $redak[1];
        }
        fclose($fh);
    }

    public function display() {
        echo '<table width="100%" cellpadding="4" cellspacing="0" border="1">';
        foreach ($this->podaci as $key => $value) {
            echo '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
        }
        echo '<tr><td colspan="2"><a href="?b=tecajna&a=opsirnije">Opširnije</a></td></tr>';
        echo '</table>';
     
    }
    
    public static function sve(){
        $sve = 'EUR,BLH,AFG,IRY,USD,CHF,AUD,GBP';
       return $sve;
    }

}
?>
