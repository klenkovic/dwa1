﻿<?php

class Dionice
{
	private $path = 'files/dionice.txt';
	private $podaci = array();
        
	public function __construct() {
            $dat=fopen($this->path, 'r');
            while($red=fgets($dat))
                {
			$redak=explode("\t",$red);
			$this->podaci[]=array($redak[1],$redak[2]);
                       
		}
		fclose($dat);
        }
	
	public function display()
	{
		echo '<table width="100%" cellpadding="4" cellspacing="0" border="1">';
                foreach($this->podaci as $p){
                    echo '<tr><td>'.$p[0].'</td><td>'.$p[1].'</td></tr>';
                }
                echo '</table>';
	}
}

?>