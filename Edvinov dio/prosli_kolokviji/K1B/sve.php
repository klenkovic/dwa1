<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php

if(isset($_GET['akcija']))
{
	$a = $_GET['akcija'];
}
else
{
	$a = '';
}

switch($a)
{
	case 'login': login_php(); break;
	case 'obrada': obrada(); break;
	default: login_htm();	
}

function login_htm()
{
	echo '<form name="form1" method="post" action="?akcija=login">
  <p>Prijava</p>
  <p>Korisničko ime: 
    <input type="text" name="username">
  </p>
  <p>Password: 
    <input type="password" name="password">
  </p>
  <p>
    <input name="prijava" type="submit" value="Prijava">
  </p>
</form>';
}

function login_php()
{
	$ime = $_POST['username'];
	$loz = trim($_POST['password']);
	$bingo = false;
	$path = './korisnici.txt';
	
	if (empty($ime) || empty($loz))
	{
		echo '<p>Username i/ili password su prazni. Pokušajte ponovno</p>
			<a href="?akcija=">Povratak</a>';
	}
	else
	{
		if(!empty($path))
		{
			$fh = fopen($path, 'r');
			while(($red = fgets($fh, 4096)) !== false)
			{
				$redak = explode("\t", $red);
				if($redak[0] == $ime && trim($redak[1]) == $loz)	$bingo = true;	
			}
			fclose($fh);
		}
		else
		{
			echo '<p>Greska</p>';	
		}
		
		if($bingo)
		{
			echo 'Dobrodošao/la '.$ime;
		}
		else
		{
			echo '<p>Neispravni podaci. Pokusajte ponovno</p>';
			echo '<a href="?akcija=">Povratak</a>';
		}
	}
	
	if ($bingo)
	{
		echo '<h2>Odaberi proizvod</h2>';
		echo '<form name="form2" method="post" action="?akcija=obrada">
		<select name="proizvod" id="proizvod">
		  <option value="T-shirt">T-shirt – 100kn</option>
		  <option value="Trenerka">Trenerka – 200kn</option>
		  <option value="Traperice">Traperice – 300kn</option>
		</select>
		<select name="velicina" id="velicina">
		  <option value="LARGE">LARGE-100kn</option>
		  <option value="EXTRALARGE">EXTRALARGE-200kn</option>
		  <option value="XXL">XXL-300kn</option>
		</select>
		<input type="submit"  name="submit2" value="Submit">
		<input type="hidden" name="id" value="'.$ime.'">
		</form>';
	}
}

function obrada()
{
$cijena = 0;
$proizvod = $_POST['proizvod'];
$velicina = $_POST['velicina'];
$imeS = $_POST['id'];
$path = './narudzbe.txt';
$ok = false;

switch($proizvod)
{
	case 'T-shirt': $cijena += 100; break;
	case 'Trenerka': $cijena += 200; break;
	case 'Traperice': $cijena += 300; break;
	default: echo '<p>Greska</p>';	
}

switch($velicina)
{
	case 'LARGE': $cijena += 100; break;
	case 'EXTRALARGE': $cijena += 200; break;
	case 'XXL': $cijena += 300; break;
	default: echo '<p>Greska</p>';	
}

echo 'Ukupna cijena: '.$cijena.' kn<br>';

define('SEP', "\t");
$redak = $imeS.SEP.$proizvod.SEP.$velicina.SEP.$cijena.SEP.date('d-m-Y')."\n";

if (is_writeable($path)) $ok = true;

if ($ok)
{
	$fh = fopen($path, 'a');
	flock($fh, LOCK_EX);
	fwrite($fh, $redak);
	flock($fh, LOCK_UN);
	fclose($fh);
	echo '<p>Uspješan unos</p>';
}

if(file_exists($path))
{
	if(is_readable($path))
	{
		$fh = fopen($path, 'r');
		echo '<table border="1">';
		while(($red = fgets($fh, 4096)) !== false)
		{
			$redak = explode("\t", $red);
			echo '<tr>';
			foreach($redak as $r)
			{
				echo '<td>'.$r.'</td>';
			}
			echo '</tr>';
		}
		echo '</table>';
		fclose($fh);
		}
	}
	
	echo '<a href="?akcija=">Početak</p>';
}

?>
</body>
</html>