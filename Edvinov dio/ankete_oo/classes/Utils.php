<?php

final class Utils {
    
    public static function linkTo($path, $args, $text, $class = ''){
        $rez = '';
        $rez.='<a href="' . $path;
        if (!empty($args)) {
            $rez.='?';
            $br = 1;
            foreach ($args as $key => $value) {
                $rez.=$key . '=' . $value;
                if ($br < count($args)) {
                    $rez.='&';
                }
            }
        }
        $rez.='"';
        if (strlen($class) > 0) {
            $rez.=' class="' . $class . '" ';
        }
        $rez.='>' . $tekst . '</a>';

    return $rez;
    }
    
    
    public static function select($path,$id,$fields,$name){
    if(file_exists($path))// DA LI POSTOJI DATOTEKA?
	{
		if(is_readable($path))// MOGU LI JE JA ČITATI?
		{
			if(filesize($path)>0)// JE LI PRAZAN?
			{

				// CITANJE IZ TXT DATOTEKE
				$fh = fopen($path,'r');
                                $rez = '';
                                $rez.='<select name="'.$name.'">';
				while (($red = fgets($fh, 4096)) !== false) 
				{	
                                    $redak = explode("\t",$red);
                                    $rez.='<option value="'.$redak[$id].'">';
                                    if(is_array($fields)){
                                        foreach ($fields as $f) {
                                            $rez.= $redak[$f].' ';
                                        }
                                    } else { $rez.= $redak[$fields]; }
                                    $rez.='</option>';
				}
                                $rez.='</select>';
				fclose($fh); 
                                return $rez;
			} 
                        else { return false; }
		} else { return false; }
	} else { return false; }
        
    }
    public static function preselect($path,$id,$value,$fields,$name){
        if(file_exists($path))
	{
		if(is_readable($path))
		{
			if(filesize($path)>0)
			{

				$fh = fopen($path,'r');
                                $rez = '';
                                $rez.='<select name="'.$name.'">';
				while (($red = fgets($fh, 4096)) !== false) 
				{	
                                    $redak = explode("\t",$red);
                                    $rez.='<option value="'.$redak[$id];
                                    if($redak[$id]==$value){ $rez.=' selected '; }
                                    $rez.='">';
                                    if(is_array($fields)){
                                        foreach ($fields as $f) {
                                            $rez.= $redak[$f].' ';
                                        }
                                    } else { $rez.= $redak[$fields]; }
                                    $rez.='</option>';
				}
                                $rez.='</select>';
				fclose($fh); 
                                return $rez;
			} 
                        else { return false; }
		} else { return false; }
	} else { return false; }
    }
}
?>
