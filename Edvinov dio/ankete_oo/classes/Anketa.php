<?php

class Anketa{
    private $id;
    private $pitanje;
    public $o1;
    public $o2;
    public $o3;
    public $o4;
    public $go1;
    public $go2;
    public $go3;
    public $go4;
    
    public function __construct($redak='') {
       if($redak!=''){
           $this->id = $redak[0];
           $this->pitanje = $redak[1];
           $this->o1 = $redak[2];
           $this->o2 = $redak[3];
           $this->o3 = $redak[4];
           $this->o4 = $redak[5];
           $this->go1 = $redak[6];
           $this->go2 = $redak[7];
           $this->go3 = $redak[8];
           $this->go4 = $redak[9];
       }
    }
    
    public function getPitanje(){
        return $this->pitanje;
    }
    public function setPitanje($p){
        $this->pitanje = $p;
    }	
	
	public function getId(){
        return $this->id;
    }
	
    public function display(){
        echo '<form action="?a=unesi" method="post">';
		echo '<h1>'.$this->pitanje.'</h1>';
		echo '<p><input type="radio" name="anketa" value="1">'.$this->o1.' ---> '.$this->og1.'</p>';
		echo '<p><input type="radio" name="anketa" value="2">'.$this->o2.' ---> '.$this->og2.'</p>';
		echo '<p><input type="radio" name="anketa" value="3">'.$this->o3.' ---> '.$this->og3.'</p>';
		echo '<p><input type="radio" name="anketa" value="4">'.$this->o4.' ---> '.$this->og4.'</p>';
		echo '<p><input type="submit" name="Submit" value="Glasaj!"></p>';
		echo '<input type="hidden" name="id" value="'.$this->id.'">';
		echo '</form>';
    }
    
    public static function form(){
         echo '<form name="form1" method="post" action="?a=insert">
		<fieldset><legend>Unos nove ankete</legend>&nbsp;</p>
		<p>Pitanje: 
			<input name="pitanje" type="text" id="pitanje" size="60">
		</p>
		<p>Odgovor 1: 
			<input name="o1" type="text" id="o1">
		</p>
		<p>Odgovor 2: 
			<input name="o2" type="text" id="o2">
		</p>
		<p>Odgovor 3: 
			<input name="o3" type="text" id="o3">
		</p>
		<p>Odgovor 4: 
			<input name="o4" type="text" id="o4">
		</p>
		<p>
			<input type="submit" name="Submit" value="Submit">
		</p>
		</fieldset>
		</form>';
    }
 
}
?>
