﻿<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>

<?php
$path = '/home/ijugo/anketa.txt';
// Postoji li datoteka?
if(file_exists($path))
{
	// Mogu li je čitati?
	if(is_readable($path))
	{
		if(filesize($path)!=0)
		{
			// CITANJE IZ TXT DATOTEKE
			$fh = fopen($path, 'r');
			
			echo '<table border="1">';
			while(($red = fgets($fh, 4096)) !== false)
			{
				$redak = explode("\t", $red);
				echo '<tr>';
				foreach($redak as $r)
				{
					echo '<td>'.$r.'</td>';
				}
				echo '</tr>';
			}
			echo '</table>';
			
			fclose($fh);
		}
		else
		{
			echo 'Necu se spajati kad je prazan';
		}
	}
	else
	{
		echo 'Ne mogu čitati datoteku';
	}
}
else
{
	echo 'Datoteka ne postoji';
}
?>

</body>
</html>
