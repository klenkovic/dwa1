<?php
function save($path, $sadrzaj, $mode) {
    if (file_exists($path)) {
        if (is_writeable($path)) {
            if ($mode == 'a' || $mode == 'w') {

                $fh = fopen($path, $mode);
                flock($fh, LOCK_EX);
                fwrite($fh, $sadrzaj);
                flock($fh, LOCK_UN);
                fclose($fh);
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}
?>