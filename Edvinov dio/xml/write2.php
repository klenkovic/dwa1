<?php
// PUTANJA I IME DATOTEKE
$file = "books.xml";
// OTVORI FILE
$fp = fopen($file, "rb") or die("cannot open file");
// POKUPI SADRZAJ
$str = fread($fp, filesize($file));

// RADI SE MODIFICIRANJE SADRZAJA
// MORAMO IMATI ORIGINAL U VARIJABLI $str, A MIJENJAMO DOM

// STVORI PRAZNI XML DOM OBJEKT U MEMORIJI
$xml = new DOMDocument();
$xml->formatOutput = true;
$xml->preserveWhiteSpace = false;
// NAPUNI GA PODACIMA IZ ORIGINALNE DATOTEKE
$xml->loadXML($str) or die("Error");

// ISPISI ORIGINALNI SADRZAJ
echo "<xmp>OLD:\n". $xml->saveXML() ."</xmp>";

// UZMI OKVIRNI ELEMENT
$root   = $xml->documentElement;
// UZMI (PROCITAJ) PRVI SKUP PODATAKA (BOOK)
$fnode  = $root->firstChild;

// UZMI DRUGI PO REDU ZAPIS
$ori    = $fnode->childNodes->item(2);
	// DODAJ NOVE PODATKE
	$id     = $xml->createElement("id");
	$idText = $xml->createTextNode("3");
	$id->appendChild($idText);
	
	$title     = $xml->createElement("title");
	$titleText = $xml->createTextNode("PHP Framework");
	$title->appendChild($titleText);
	
	$author     = $xml->createElement("author");
	$authorText = $xml->createTextNode("Reza Christian");
	$author->appendChild($authorText);
	// STVORI NOVI ZAPIS U DOM OBJEKT
	$book   = $xml->createElement("book");
	$book->appendChild($id);
	$book->appendChild($title);
	$book->appendChild($author);
// UMETNI NAKON ORIGINALNOG DRUGOG ZAPISA
$fnode->insertBefore($book,$ori);
// ZAPISI U DATOTEKU I IZBACI NA EKRAN
echo "<xmp>NEW:\n". $xml->saveXML() ."</xmp>";
?>
